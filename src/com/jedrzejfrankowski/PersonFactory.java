package com.jedrzejfrankowski;

/**
 * Created by jfrankowski on 07.02.17.
 */
@FunctionalInterface
public interface PersonFactory<P extends Person> {
    P create(String firstname, String lastname);
}
