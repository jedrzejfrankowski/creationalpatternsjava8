package com.jedrzejfrankowski;

/**
 * Created by jfrankowski on 07.02.17.
 */
public class Person {
    private String firstname;
    private String lastname;

    public Person(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
}
